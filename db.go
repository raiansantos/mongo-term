package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

var mongoClient *mongo.Client

func init() {
	//cred := options.Credential{
	//	Username: os.Getenv("DB_USER"),
	//	Password: os.Getenv("DB_PASSWORD"),
	//}
	opt := options.Client()
	//opt.SetAuth(cred)
	opt.ApplyURI(fmt.Sprintf("mongodb://127.0.0.1:27017"))

	var err error
	mongoClient, err = mongo.NewClient(opt)
	if err != nil {
		log.Fatal(err)
	}

	ctx, cancel := context.WithTimeout(context.Background(), 10*time.Second)
	defer cancel()
	err = mongoClient.Connect(ctx)
	if err != nil {
		log.Fatal(err)
	}
}
