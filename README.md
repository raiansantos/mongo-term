# MONGO TERM

Mongo Term is a project that allow you to connect and easily manage your MongoDB Server using terminal.

For now it's allowing connect only in localhost on default port without authentication.

## TODO
- Create a screen to manage the connection params
- Allow receive the params via flags on the command line
- Create a screen to filter the data and select the skipped/limitted data
- Allow add, edit and delete data